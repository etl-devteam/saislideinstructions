package it.univet.saislideinstructions;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import it.univet.visionar.visionarservice.VisionARService;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private final String TAG = "APPDEBUG";
    VisionARService VARService = null;
    boolean mBound             = false;

    // Defines callbacks for service binding, passed to bindService()
    private final ServiceConnection binderConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder iBinder) {
            // We've bound to VisionARService, cast the IBinder and get VisionARService instance
            VisionARService.VARBinder binder = (VisionARService.VARBinder) iBinder;
            VARService = binder.getService();

            mBound = true;

            VARService.setTouchpadListener(ev -> {
                if (ev == VisionARService.touchpadEvent.SINGLE_TAP) {
                    Log.d(TAG, "Touchpad: Single Tap");
                } else if (ev == VisionARService.touchpadEvent.DOUBLE_TAP) {
                    Log.d(TAG, "Touchpad: Double Tap");
                } else if (ev == VisionARService.touchpadEvent.SWIPE_DOWN) {
                    runOnUiThread(() -> goToNextStep());
                    Log.d(TAG, "Touchpad: Swipe Down");
                } else {  // VisionARService.touchpadEvent.SWIPE_UP
                    runOnUiThread(() -> goToPreviousStep());
                    Log.d(TAG, "Touchpad: Swipe Up");
                }
            });

            VARService.StartTouchpad();

            VARService.setVisionARListener(new VisionARService.VisionARListener() {
                @Override
                public void onConnect() {
                    Log.d(TAG, "VisionAR connected");
                }

                @Override
                public void onDisconnect() {
                    Log.d(TAG, "VisionAR disconnected");
                }
            });

        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };


    private int arrayLength = 0;
    private String[] arrayImagesVAR;
    private String[] arrayImagesAPP;
    private String[] arrayStringsTitle;
    private String[] arrayStringsDescription;
    private int current_index = 0;
    private Button home_btn;
    private ImageButton next_btn, prev_btn;
    private ImageView step_imagView;
    private TextView title_text, description_text;
    private LinearLayout dots_layout;

    // ---------------- 'BEGIN OF PROGRAM' -------------------
    // onCreate, onDestroy, onResume, handleIntent and onNewIntent are always present in application to manage label, intent and others
    // Declaration, assignment of TextView, Button, ...
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        initView();

        initData();
    }


    @Override
    protected void onStart() {
        super.onStart();

        Intent intent = new Intent(this, it.univet.visionar.visionarservice.VisionARService.class);
        bindService(intent, binderConnection, Context.BIND_AUTO_CREATE);
    }


    @Override
    protected void onStop() {
        super.onStop();

        if (mBound) VARService.StopTouchpad();

        mBound = false;
        unbindService(binderConnection);
    }


    private void initView() {
        dots_layout = findViewById(R.id.dotsLayout);

        prev_btn = findViewById(R.id.prev_button);
        next_btn = findViewById(R.id.next_button);
        home_btn = findViewById(R.id.home_button);
        step_imagView = findViewById(R.id.step_image);
        title_text = findViewById(R.id.title_step);
        description_text = findViewById(R.id.description_step);

        prev_btn.setOnClickListener(this);
        next_btn.setOnClickListener(this);
        home_btn.setOnClickListener(this);
    }

    private void initData() {

        arrayLength = getResources().getIntArray(R.array.app_images).length;

        arrayImagesVAR = new String[arrayLength];
        arrayImagesAPP = new String[arrayLength];
        arrayStringsTitle = new String[arrayLength];
        arrayStringsDescription = new String[arrayLength];

        for(int i = 0; i < arrayLength; i++) {
            arrayImagesVAR[i] = getResources().getStringArray(R.array.var_images)[i];
            arrayImagesAPP[i] = getResources().getStringArray(R.array.app_images)[i];
            arrayStringsTitle[i] = getResources().getStringArray(R.array.title_strings)[i];
            arrayStringsDescription[i] = getResources().getStringArray(R.array.description_strings)[i];
        }

        createDots(current_index);
        current_index = -1;
    }


    private void createDots(int current_position) {
        if(dots_layout != null){
            dots_layout.removeAllViews();
        }
        ImageView[] dots = new ImageView[arrayLength];

        for(int i = 0; i < arrayLength; i++){
            dots[i] = new ImageView(this);
            if(i == current_position){
                dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.active_dots));
            }else{
                dots[i].setImageDrawable((ContextCompat.getDrawable(this, R.drawable.inactive_dots)));
            }

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(4, 0, 4, 0);

            dots_layout.addView(dots[i], params);
        }
    }


    @Override
    public void onClick(View view) {

        if(view == prev_btn) {
            goToPreviousStep();
        } else if(view == next_btn) {
            goToNextStep();
        } else if(view == home_btn) {
            goToFirstStep();
        }
    }


    private void goToNextStep() {
        current_index++;

        if(current_index > arrayLength - 1) {
            current_index = 0;
        }

        loadContentInStep(current_index);
        createDots(current_index);
    }


    private void goToPreviousStep() {
        current_index--;

        if(current_index < 0) {
            current_index = arrayLength - 1;
        }

        loadContentInStep(current_index);
        createDots(current_index);
    }


    private void goToFirstStep() {
        current_index = 0;

        loadContentInStep(current_index);
        createDots(current_index);
    }


    private void loadContentInStep(int index) {
        int imgIDVAR = getResources().getIdentifier(arrayImagesVAR[index], "drawable", getPackageName());
        int imgIDAPP = getResources().getIdentifier(arrayImagesAPP[index], "drawable", getPackageName());
        // BODY
        if (VARService.WriteImg(BitmapFactory.decodeResource(getResources(), imgIDVAR), VisionARService.includeBattery.INCLUDE) == VisionARService.writeStatus.SUCCESS) {
            Log.d("@ @ @ ", "writeImage SUCCESS");
            step_imagView.setImageResource(imgIDAPP);
            title_text.setText(arrayStringsTitle[index]);
            description_text.setText(arrayStringsDescription[index]);
        } else Log.d("@ @ @ ", "writeImage FAILED");
    }
}
